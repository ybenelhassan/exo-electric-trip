package com.nespresso.exercise.electric_trip;

public class Trip {

	private String sourceCity;
	private String destinationCity;
	private int distance;
	
	public Trip(String sourceCity, String destinationCity, int distance) {
		this.sourceCity = sourceCity;
		this.destinationCity = destinationCity;
		this.distance = distance;
	}
	
	public boolean canTripStartFromCity(final String sourceCity) {
		return this.sourceCity.equalsIgnoreCase(sourceCity);
	}

	public void printDestinationCity(StringBuilder builder) {
		builder.append(this.destinationCity);
	}

	public int calculateSpentEnergy(int performance) {
		return distance / performance;
	}
}
