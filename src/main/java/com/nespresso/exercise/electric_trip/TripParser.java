package com.nespresso.exercise.electric_trip;

import java.util.List;

public interface TripParser {

	public List<Trip> parseTrips(String tripsPaths);
	
}
