package com.nespresso.exercise.electric_trip;

import java.util.ArrayList;
import java.util.List;

public class ElectricTripParser implements TripParser, ParticipantParser{

	private static final String SEPARATOR = "-";
	
	public List<Trip> parseTrips(String tripsPaths) {
		List<Trip> trips = new ArrayList<Trip>();
		String[] tripsParts = tripsPaths.split(SEPARATOR);
		
		int tripPartslength = tripsParts.length;
		for(int i = 0 ; i < tripPartslength ; i+=2) {
			if(i < tripPartslength - 1) {
				int sourceCityIndex = i;
				int distanceIndex = i + 1;
				int destinationCityIndex = i + 2;
				parseTrip(trips, tripsParts, sourceCityIndex, distanceIndex,
						destinationCityIndex);
			}
		}
		
		return trips;
	}

	public Participant parseParticipant(String sourceCity, int batterySize,
			int lowSpeedPerformance, int highSpeedPerformance, List<Trip> trips) {
		List<Trip> tripsForParticipant = new ArrayList<Trip>();
		boolean startCityAlreadyFound = false;
		for(Trip trip : trips) {
			startCityAlreadyFound = addTripForParticipant(sourceCity,
					tripsForParticipant, startCityAlreadyFound, trip);
		}
		Participant participant = new Participant(tripsForParticipant, batterySize, lowSpeedPerformance, highSpeedPerformance);
		return participant;
	}

	private boolean addTripForParticipant(String sourceCity,
			List<Trip> tripsForParticipant, boolean startCityAlreadyFound,
			Trip trip) {
		if(trip.canTripStartFromCity(sourceCity) || startCityAlreadyFound) {
			tripsForParticipant.add(trip);
			startCityAlreadyFound = true;
		}
		return startCityAlreadyFound;
	}

	private void parseTrip(List<Trip> trips, String[] tripsParts,
			int sourceCityIndex, int distanceIndex, int destinationCityIndex) {
		String sourceCity = tripsParts[sourceCityIndex];
		int distance = Integer.parseInt(tripsParts[distanceIndex]);
		String destinationCity = tripsParts[destinationCityIndex];
		
		Trip trip = new Trip(sourceCity, destinationCity, distance);
		trips.add(trip);
	}
}
