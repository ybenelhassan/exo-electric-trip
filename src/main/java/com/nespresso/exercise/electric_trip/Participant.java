package com.nespresso.exercise.electric_trip;

import java.util.List;

public class Participant {

	private static final String PERCENT_SYMBOL = "%";
	private static final int PERCENT_VALUE = 100;
	
	private List<Trip> trips;
	
	private int batterySize;
	private int availableEnergy;
	private int lowSpeedPerformance;
	private int highSpeedPerformance;
	private int currentTrip;
	
	public Participant(List<Trip> trips, int batterySize, int lowSpeedPerformance,
			int highSpeedPerformance) {
		this.trips = trips;
		this.batterySize = batterySize;
		this.lowSpeedPerformance = lowSpeedPerformance;
		this.highSpeedPerformance = highSpeedPerformance;
		this.availableEnergy = PERCENT_VALUE;
		
		//not trip started yet
		this.currentTrip = -1;
	}
 
	public void go() {
		this.go(this.lowSpeedPerformance);
	}

	public void goUsingHightSpeed() {
		this.go(this.highSpeedPerformance);
	}
	
	private void go(int usedPerformace) {
		int availableBatterySize = calculateAvailableBatterySize();
		
		for (Trip trip : trips) {
			int requiredBatterySizeForTrip = startTripIfEnergyIsAvailable(availableBatterySize, trip, usedPerformace);
			availableBatterySize -= requiredBatterySizeForTrip;
		}
		float availableBatterySizePerCent = availableBatterySize * PERCENT_VALUE;
		this.availableEnergy = Math.round(availableBatterySizePerCent / batterySize);
	}
	
	public void printLocation(StringBuilder builder) {
		Trip currentTrip = trips.get(this.currentTrip);
		currentTrip.printDestinationCity(builder);
	}

	public void printAvailableEnergy(StringBuilder builder) {
		builder.append(availableEnergy).append(PERCENT_SYMBOL);
	}
	
	private int calculateAvailableBatterySize() {
		float perCent = PERCENT_VALUE;
		float availableSizePerCent = this.availableEnergy / perCent;
		return Math.round(availableSizePerCent * this.batterySize);
	}

	private int startTripIfEnergyIsAvailable(int availableBatterySize, Trip trip, int performance) {
		int requiredBatterySize = trip.calculateSpentEnergy(performance);
		if(availableBatterySize >= requiredBatterySize) {
			this.currentTrip++;
			return requiredBatterySize;
		}
		return 0;
	}

}
