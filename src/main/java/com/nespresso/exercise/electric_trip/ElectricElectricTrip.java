package com.nespresso.exercise.electric_trip;

import java.util.ArrayList;
import java.util.List;

public class ElectricElectricTrip {

	List<Trip> trips;
	List<Participant> participants;
	
	public ElectricElectricTrip(final String tripsPaths) {
		//parse trips
		ElectricTripParser tripParser = new ElectricTripParser(); 
		this.trips = tripParser.parseTrips(tripsPaths);
		
		this.participants = new ArrayList<Participant>();
	}

	public int startTripIn(final String sourceCity, final int batterySize,
			final int lowSpeedPerformance, final int highSpeedPerformance) {

		ParticipantParser participantParser = new ElectricTripParser();
		Participant participant = participantParser.parseParticipant(sourceCity, batterySize, lowSpeedPerformance, highSpeedPerformance, trips);
		
		participants.add(participant);
		int participantId = participants.size() - 1;
		return participantId;
	}

	public void go(int participantId) {
		if(participants.size() > participantId) {
			Participant participant = participants.get(participantId);
			participant.go();
		}
	}

	public void sprint(int participantId) {
		if(participants.size() > participantId) {
			Participant participant = participants.get(participantId);
			participant.goUsingHightSpeed();
		}
	}
	
	public String locationOf(int participantId) {
		StringBuilder builder = new StringBuilder();
		Participant participant = participants.get(participantId);
		participant.printLocation(builder);
		return builder.toString();
	}

	public String chargeOf(int participantId) {
		StringBuilder builder = new StringBuilder();
		Participant participant = participants.get(participantId);
		participant.printAvailableEnergy(builder);
		return builder.toString();
	}

	public void charge(int id, int hoursOfCharge) {
		// TODO Auto-generated method stub
		
	}

}
