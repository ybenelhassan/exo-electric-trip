package com.nespresso.exercise.electric_trip;

import java.util.List;


public interface ParticipantParser{

	public Participant parseParticipant(String sourceCity, int batterySize,
			int lowSpeedPerformance, int highSpeedPerformance, List<Trip> trips);

		
}
